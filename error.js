/* eslint-disable max-classes-per-file */

// -----------------------------------------------------------------------------

class ErrorCode extends Error {
	constructor(statusCode = 500, reason, userSafeMessage) {
		super(reason);

		this.statusCode = statusCode;
		this.userSafeMessage = userSafeMessage;
	}
}

// -----------------------------------------------------------------------------

class Error400 extends ErrorCode {
	constructor(reason, userSafeMessage) {
		super(400, reason, userSafeMessage);
	}
}

class Error401 extends ErrorCode {
	constructor(reason, userSafeMessage) {
		super(401, reason, userSafeMessage);
	}
}

class Error403 extends ErrorCode {
	constructor(reason, userSafeMessage) {
		super(403, reason, userSafeMessage);
	}
}

class Error404 extends ErrorCode {
	constructor(reason, userSafeMessage) {
		super(404, reason, userSafeMessage);
	}
}

class Error405 extends ErrorCode {
	constructor(reason, userSafeMessage) {
		super(405, reason, userSafeMessage);
	}
}

class Error409 extends ErrorCode {
	constructor(reason, userSafeMessage) {
		super(409, reason, userSafeMessage);
	}
}

// -----------------------------------------------------------------------------

class Error500 extends ErrorCode {
	constructor(reason, userSafeMessage) {
		super(500, reason, userSafeMessage);
	}
}

class Error501 extends ErrorCode {
	constructor(reason, userSafeMessage) {
		super(501, reason, userSafeMessage);
	}
}

// -----------------------------------------------------------------------------

module.exports = {
	Error400,
	Error401,
	Error403,
	Error404,
	Error405,
	Error409,
	Error500,
	Error501,
};
