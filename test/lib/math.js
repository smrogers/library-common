/* eslint codemetrics/complexity: 0 */

const {expect} = require('chai');

const {clamp} = require('../../math');

// -------------------------------------------------------------------------

describe('math.js', () => {
	describe('clamp', () => {
		it('should return original value', () => {
			expect(clamp(50, 40, 60)).to.equal(50);
		});

		it('should return minimum value', () => {
			expect(clamp(0, 40, 60)).to.equal(40);
		});

		it('should return maximum value', () => {
			expect(clamp(100, 40, 60)).to.equal(60);
		});
	});
});
