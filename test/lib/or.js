/* eslint codemetrics/complexity: 0 */

const {expect} = require('chai');

const {orArr, orObj, orValue} = require('../../or');

// -------------------------------------------------------------------------

describe('or.js', () => {
	const test = 'test';
	const value = 'value';
	const arr = [];
	const obj = {};

	describe('orArr', () => {
		it('should return value', () => {
			expect(orArr(arr)).to.equal(arr);
			expect(orArr(arr)).to.equal(arr);
		});
		it('should return defaultValue', () => {
			expect(JSON.stringify(orArr(undefined))).to.equal('[]');
			expect(JSON.stringify(orArr(false))).to.equal('[]');
			expect(JSON.stringify(orArr(0))).to.equal('[]');
			expect(JSON.stringify(orArr(''))).to.equal('[]');
		});
	});

	describe('orObj', () => {
		it('should return value', () => {
			expect(orObj(obj)).to.equal(obj);
			expect(orObj(obj)).to.equal(obj);
		});
		it('should return defaultValue', () => {
			expect(JSON.stringify(orObj(undefined))).to.equal('{}');
			expect(JSON.stringify(orObj(false))).to.equal('{}');
			expect(JSON.stringify(orObj(0))).to.equal('{}');
			expect(JSON.stringify(orObj(''))).to.equal('{}');
		});
	});

	describe('orValue', () => {
		it('should return value', () => {
			expect(orValue(test)).to.equal(test);
			expect(orValue(test, value)).to.equal(test);
			expect(orValue(arr, value)).to.equal(arr);
			expect(orValue(obj, value)).to.equal(obj);
		});

		it('should return defaultValue', () => {
			expect(orValue(undefined, value)).to.equal(value);
			expect(orValue(false, value)).to.equal(value);
			expect(orValue(0, value)).to.equal(value);
			expect(orValue('', value)).to.equal(value);
		});
	});
});
