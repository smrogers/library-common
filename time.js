module.exports = {
	duration,
};

// -----------------------------------------------------------------------------

function duration() {
	const start = Date.now();

	return () => Date.now() - start;
}
