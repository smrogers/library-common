module.exports = function log(...items) {
	// eslint-disable-next-line no-console
	console.log(new Date().toISOString(), ...items);
};
